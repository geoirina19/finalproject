import { Component, OnInit } from '@angular/core';
import { Oneproduct } from './oneproduct';
import { HttpClient } from '@angular/common/http';
import { ProductService } from './products.service';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ ProductService ]
})

export class ProductsComponent implements OnInit {

  products: Oneproduct[] = [];
  page : number = 0;
  loadedAll : boolean = false;

  constructor(private productService: ProductService) {
  }

  ngOnInit() {
    this.loadBooks();
  }

  loadBooks() {
    this.productService.getBooks(this.page).subscribe(result => {
      console.log('result is ', result);
      if(result['status'] === 'success') {
        console.log(result['data']);
        if (result['data'].length == 0) {
          this.loadedAll = true;
        }
        for (let book of result['data']) {
          let p : Oneproduct = new Oneproduct();
          p.name = book["name"];
          p.author = book["author"];
          p.im = book["im"];
          p.price = book["price"];
          this.products.push(p);
        }
        this.page++;
      } else {
        console.log(result['message']);
        alert("can't load books");
      }
       
      }, error => {
        console.log('error is ', error);
      });
  }

}
