const MongoClient = require("mongodb").MongoClient; //mongoclient - ключевой класс, через который идет взаимодействие с хранилищем данных
   
const url = "mongodb+srv://ira:ira@cluster0-npljt.mongodb.net/test?retryWrites=true";
const mongoClient = new MongoClient(url, { useNewUrlParser: true });
 
let users = [{name: "Harry Potter", username: "Harry11", password: "qwerty"} , {name: "Ron Whesley", username: "Ron11", password: "qwerty11"}, {name: "Hermiona Granger", username: "Hermi11", password: "qwerty111"}];

mongoClient.connect(function(err, client){
    console.log(err);
    const db = client.db("usersdb");
    const collection = db.collection("users");
    collection.insertMany(users, function(err, results){
              
        console.log(results);
        client.close();
    });
});